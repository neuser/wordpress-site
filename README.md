## Deploy instructions

  - Run with command (in docker-compose.yml): certonly --webroot --webroot-path=/var/www/html --email gelion333@yandex.ru --agree-tos --no-eff-email --staging -d neuser.ru -d www.neuser.ru to test certbot configuration parameters.
  - environment variables must be defined in .env file
